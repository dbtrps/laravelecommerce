<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/catalog', 'ItemController@index');


Route::get('/additem', 'ItemController@create');

Route::post('/additem', 'ItemController@store');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// to delete
Route::delete('/catalog/{id}', 'ItemController@destroy');

// to go to edit form
Route::get('/edititem/{id}', 'ItemController@edit');

// to save updated items
Route::patch('/edititem/{id}', 'ItemController@update');

// cart CRUD
Route::post('/addtocart/{id}', 'ItemController@addtocart');

Route::get('/showcart', 'ItemController@showCart');