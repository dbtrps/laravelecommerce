<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       \DB::table('items')->delete();

       \DB::table('items')->insert(array(

        	
        	0 =>
        	array (
        		'id' => 1, 
        		'name' => 'name',
        		'price' => '500', 
        		'description' => 'nice',
        		'imgPath' => 'Random string',
                'category_id' => 1,
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	
        	1 =>
        	array (
        		'id' => 2,
        		'name' => 'name',
        		'price' => '600',
        		'description' => 'aweseome',
        		'imgPath' => 'random String',
                'category_id' => 1,
        		'created_at' => NULL,
        		'updated_at' => NULL
        	)
        ));
    }
}
