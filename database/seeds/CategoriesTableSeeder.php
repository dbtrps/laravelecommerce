<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array(
               
            0 =>
            array (
                'id' => 1,
                'name' => 'Nike',
                'created_at' => NULL,
                'updated_at' => NULL
            )
        ));   
    }
}
