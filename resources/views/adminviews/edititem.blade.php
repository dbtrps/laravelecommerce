@extends('layouts.app')
@section('content')

<h1 class="text-center">Edit item</h1>
<div class="col-lg-4 offset-lg-4">
	<div class="card">
		<form class="p-5" method="POST" action="/edititem/{{$item->id}}" enctype="multipart/form-data">
			@csrf
			@method('PATCH')
			<div class="form-group">
				<label>Item Name</label>
				<input type="text" name="name" class="form-control" value="{{$item->name}}">
			</div>
			<div class="form-group">
				<label>Item Description</label>
				<input type="text" name="description" class="form-control" value="{{$item->description}}">
			</div>
			<div class="form-group">
				<label>Price</label>
				<input type="number" name="price" class="form-control" value="{{$item->price}}">
			</div>
			<div class="form-group">
				<label>Image</label>
				<input type="file" name="imgPath" class="form-control">	
			</div>
			<div class="form-group">
				<select class="form-control" name="category_id">
					@foreach($categories as $indiv_category)
						<option value="{{$indiv_category->id}}" {{$indiv_category->id == $item->category_id ? "selected" : ""}}>{{$indiv_category->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="text-center">
				<button class="btn btn-success">EDIT</button>
			</div>
		</form>	
	</div>
</div>
@endsection