@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Collection</h1>
@if(Session::has("message"))
<h4>{{Session::get('message')}}</h4>
@endif

<div class="row w-100">
	
	@foreach($items as $indiv_item)
		
		<div class="col-lg-6 p-3 my-2">
			<div class="card">
				<img src="{{$indiv_item->imgPath}}" class="card-img-top" alt="nothing" height="300px">
				<div class="card-body text-center">
					<h4 class="card-title">
						<p class="card-text">{{$indiv_item->name}}</p>
						<p class="card-text">{{$indiv_item->price}}</p>
						<p class="card-text">{{$indiv_item->description}}</p>
					</h4>
				</div>
				<form action="/catalog/{{$indiv_item->id}}" method="POST">
					@csrf
					@method('DELETE')
					<div class="text-center">
						<button class="btn btn-danger">DELETE</button>
					</div>
				</form>
				<a href="/edititem/{{$indiv_item->id}}" class="btn">EDIT</a>
			</div>
			<div class="card-footer">
				<form action="/addtocart/{{$indiv_item->id}}" method="POST">
					@csrf
					<input type="number" name="quantity" class="form-control" value="1">
					<button class="btn btn-primary" type="submit">Add to Cart</button>
				</form>
			</div>
		</div>
	@endforeach
</div>
@endsection
